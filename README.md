Cloud Endpoints - Spring Framework POC
==================

POC (proof of concept) application integrating with a [Cloud Endpoints project](https://bitbucket.org/lloal/spring-cloud-endpoint-poc).

## Authors ##

Alvin Llobrera <alvin.llobrera@cloudsherpas.com>

Jordan Duabe <jordan.duabe@cloudsherpas.com>

