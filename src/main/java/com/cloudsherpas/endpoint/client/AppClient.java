package com.cloudsherpas.endpoint.client;

import com.cloudsherpas.springEndpointPOC.SpringEndpointPOC;
import com.cloudsherpas.springEndpointPOC.model.CustomerDTO;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

public class AppClient {

    private static SpringEndpointPOC poc = null;

    public static void main(String[] args) {
        SpringEndpointPOC.Builder builder = new SpringEndpointPOC.Builder(new NetHttpTransport(), new GsonFactory(), null);
        builder.setApplicationName("JavaClient");

        poc = builder.build();
    }

    public static void put(CustomerDTO customer) {
        try {
            poc.customer().put(customer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Long key) {
        try {
            poc.customer().delete(key).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
